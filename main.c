/*
 * cnano.c
 *
 * Created: 26/10/2022 17:21:16
 * Author : egil
 */ 

#include <avr/io.h>
#include "libavr_cnano/cnano.h"

int main(void)
{
	CLKCTRL_max_frequency();
	
	LED0_enable();

	SW0_enable(PORT_ISC_RISING_gc); // Just to test an interrupt setting first?

	SW0_enable(PORT_ISC_INTDISABLE_gc);
	
    /* Replace with your application code */
    while (1) 
    {
		if(SW0_state()) {
			LED0_on();
		} else {
			LED0_off();
		}
    }
}

#include <avr/interrupt.h>

ISR(SW0_vect) {
	/* Add your application code */
	LED0_toggle();
	
	SW0_intflag_clear();
}

